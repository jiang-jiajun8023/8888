
import time

from selenium import webdriver
from selenium.webdriver.common.by import By

class TestAa():
  def setup_method(self, method):
    self.driver = webdriver.Chrome()
    self.vars = {}
  
  def teardown_method(self, method):
    self.driver.quit()
  
  def test_aa(self):
    self.driver.get("https://ceshiren.com/")
    time.sleep(3)
    self.driver.set_window_size(945, 1012)
    self.driver.find_element(By.LINK_TEXT, "欢迎光临测试人社区 | Powered by 霍格沃兹测试开发学社").click()
    assert self.driver.find_element(By.LINK_TEXT, "欢迎光临测试人社区 | Powered by 霍格沃兹测试开发学社").text == "欢迎光临测试人社区 | Powered by 霍格沃兹测试开发学社"
  
