
import pytest
import allure



def add(a, b):
     if type(a) != int and type(a) != float:
        return "不是整数或者浮点数"
     elif type(b) != int and type(b) != float:
        return "不是整数或者浮点数"
     elif a < -99 or a > 99 :
        return "不在范围内"
     elif b < -99 or  b > 99:
        return "不在范围内"

     else:
        return a + b

def setup_function():
    print("开始计算")


def test_add_two_int():
    a, b = 11, 22

    result = add(a, b)
    assert result == a + b

def test_add_three_type():
    a, b = "asd", "ads"

    assert "不是整数或者浮点数" == add(a, b)

def test_add_four_float():
    m, n = 9.0, 88.0
    resul = add(m, n)
    assert resul == m + n




